SELECT maker,type FROM Product
ORDER BY maker;

SELECT model,ram,screen,price FROM Laptop
WHERE price>1000
ORDER BY ram DESC,price;

SELECT * FROM Printer WHERE color='n'
ORDER BY -price;

SELECT model,speed,hd,cd,price FROM PC
WHERE (cd='12x')OR(cd='24x')AND price<600
ORDER BY price ;

SELECT name,class FROM Ships
ORDER BY name;

SELECT * FROM PC
WHERE speed>=500 and price<800
ORDER BY -price;

SELECT * FROM Printer
WHERE type !='Matrix' and price<300 and type='Matrix'
ORDER BY type;

SELECT model,speed,price FROM PC
WHERE price BETWEEN 400 AND 600
ORDER BY hd;

SELECT * FROM PC
WHERE hd between 10 and 20 and ram =128;

SELECT model,speed,hd, price FROM Laptop
WHERE screen>12
ORDER BY price;

SELECT model FROM PC
WHERE model  REGEXP '11';

SELECT * FROM Outcome
WHERE month(date) =03;

SELECT * FROM Outcome
WHERE day(date) = 14;

SELECT name FROM Ships
WHERE name LIKE 'W%n';

SELECT name FROM Ships
WHERE name LIKE '%e%e%';

SELECT name,launched from Ships
WHERE name NOT RLIKE 'a$';

SELECT name FROM Battles
WHERE name LIKE '% %' AND name NOT RLIKE 'c$';

SELECT * FROM Trip
WHERE  hour(time_out) BETWEEN 12 AND 17;

SELECT * FROM Trip
WHERE  hour(time_in) BETWEEN 17 AND 2;

SELECT date FROM Pass_in_trip
WHERE place RLIKE '1';

SELECT maker,type,speed,hd FROM Product,PC
WHERE hd<8;

SELECT maker FROM PC,Product
WHERE speed>600;

SELECT maker FROM Laptop,Product
WHERE speed<500;

SELECT  DISTINCT One.country, One.class class_bb, Two.class class_bc  FROM Classes One, Classes Two
WHERE One.country = Two.country AND  One.type = 'bb' AND Two.type = 'bc' AND One.class != Two.class;

SELECT maker,PC.model from Product,pc
WHERE price<600;

SELECT Printer.model,maker FROM Printer,Product
WHERE price>300;

SELECT maker,Laptop.model,Laptop.price,PC.model,PC.price FROM Product,Laptop,PC;

SELECT maker,type,Laptop.model,speed FROM Laptop,Product
WHERE type = 'Laptop' AND speed>600;

SELECT * FROM Battles,Ships
WHERE Ships.class ='Tennessee';

SELECT name FROM Ships


SELECT name, class FROM Ships
ORDER BY name;

SELECT * FROM Classes
WHERE country = 'Japan'
ORDER BY type DESC;

SELECT name, launched FROM Ships
WHERE launched BETWEEN 1920 AND 1942
ORDER BY launched DESC;

SELECT ship, battle, result FROM Outcomes
WHERE result != 'sunk' AND battle = 'Guadalcanal'
ORDER BY ship DESC;

SELECT ship, battle, result FROM Outcomes
WHERE result = 'sunk'
ORDER BY ship DESC;

SELECT class, displacement FROM Classes
WHERE displacement > 40000
ORDER BY type;

SELECT name FROM Ships
WHERE name LIKE 'W%n';

SELECT name FROM Ships
WHERE name LIKE '%e%e%';

SELECT name, launched FROM Ships
WHERE name NOT RLIKE 'a$';

SELECT name FROM Battles
WHERE name LIKE '% %' AND  name NOT RLIKE 'c$';

SELECT  DISTINCT One.country, One.class class_bb, Two.class class_bc  FROM Classes One, Classes Two
WHERE One.country = Two.country AND  One.type = 'bb' AND Two.type = 'bc' AND One.class != Two.class;

SELECT name, displacement FROM Classes, Ships
WHERE Classes.class = Ships.class;

SELECT ship, battle, date FROM Battles, Outcomes
WHERE Battles.name = Outcomes.battle AND result = 'OK';

SELECT name, country FROM Ships,Classes
WHERE Ships.class = Classes.class;